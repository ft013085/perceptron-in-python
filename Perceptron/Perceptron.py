import numpy as np
import matplotlib.pyplot as plt

class Perceptron:
    def __init__(self, input_size, learning_rate=1, epochs=500):
        self.weights = np.random.rand(input_size + 1)  # +1 for bias
        self.learning_rate = learning_rate
        self.epochs = epochs

    def activation_function(self, x):
        return 1 if x >= 0 else 0

    def predict(self, inputs):
        summation = np.dot(inputs, self.weights[1:]) + self.weights[0]  # Adding bias
        return self.activation_function(summation)

    def train(self, training_inputs, labels):
        for _ in range(self.epochs):
            for inputs, label in zip(training_inputs, labels):
                prediction = self.predict(inputs)
                error = label - prediction
                self.weights[1:] += self.learning_rate * error * inputs
                self.weights[0] += self.learning_rate * error

    def plot_decision_boundary(self, training_inputs, labels):
        # Plotting data points
        for i in range(len(labels)):
            if labels[i] == 0:
                plt.scatter(training_inputs[i][0], training_inputs[i][1], color='red')
            else:
                plt.scatter(training_inputs[i][0], training_inputs[i][1], color='blue')

        # Plotting decision boundary
        w = self.weights[1:]
        b = self.weights[0]

        # Equation of the line: x2 = -(w1/w2)x1 + ((0.5 - w0) / w2)
        m = -w[0] / w[1]
        c = (0.5 - b) / w[1]

        x1 = np.linspace(-1, 1, 100)
        x2 = m * x1 + c
        plt.plot(x1, x2, '-g', label='Decision Boundary')
        plt.xlabel('X1')
        plt.ylabel('X2')
        plt.title('Perceptron Decision Boundary')
        plt.legend()

        # Output equation of the line
        print("Equation of the line: x2 = {:.2f}x1 + {:.2f}".format(m, c))

        plt.show()


# Example usage
training_inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
labels = np.array([0, 1, 1, 1])

perceptron = Perceptron(input_size=2)
perceptron.train(training_inputs, labels)
perceptron.plot_decision_boundary(training_inputs, labels)
